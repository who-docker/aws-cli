FROM amazon/aws-cli:latest

RUN yum update -y && yum install -y zip && yum clean all

ENTRYPOINT ["/bin/bash"]
